
// パッケージをインストール
const gulp = require("gulp");
const notify = require("gulp-notify");
const plumber = require("gulp-plumber");
const pug = require("gulp-pug");
const sass = require("gulp-sass");
const sassGlob = require("gulp-sass-glob");
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer'); 

const rename = require("gulp-rename");
const data = require("gulp-data");
const browserSync = require('browser-sync');

//pug入力先、html出力先を記述
const paths = {
  src: {
    pug:  ["src/pug/**/*.pug", "!src/pug/layout/**", "!src/pug/include/**"],
    sass: "src/sass/**/*.scss",
  },
  dest: {
    html: "./public/",
    css:  "./public/",
  },
};


//GLOBAL VARIABLES

let IS_TEST = null;
let IS_PRODUCTION = null;

//タスクを返す関数
function settingTaskFactory({isTest=false}){
  //タスク。クロージャにより設定を行う。
  return function settingTask(done){
    IS_TEST = isTest;
    IS_PRODUCTION = ! IS_TEST;

    return done();
  }
}


// sass コンパイル
function sassTask() {
  return gulp
    .src(paths.src.sass)
    // Sassのコンパイルを実行
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(postcss([autoprefixer({overrideBrowserslist: ['last 1 versions']})])) //ベンダープレフィックスを追加
    // cssフォルダー以下に保存
    .pipe(gulp.dest(paths.dest.css))
}

//pug -> htmlの名前とディレクトリを変更する
function renamePath(filepath){
  //index.pug -> index.html
  //events.pug -> events/index.html
  //events/about.pug -> events/about/index.pug
  //events__about.pug -> events/about/index.pug

  const oldDir = filepath.dirname;
  const oldBase = filepath.basename;
  const oldExt = filepath.extname;

  filepath.extname  = ".html";

  //index, inc/header, inc/footer だけは、例外。何もしない
  if(oldBase == "index"){
    return;
  }

  //通常は、 __で区切られていたらディレクトリに変換する。
  const splitedBase = oldBase.split("__");
  filepath.dirname  += "/" + splitedBase.join("/");

  //全部index.htmlに変換
  filepath.basename = "index";

  //console.log(`${path.join(oldDir, oldBase+oldExt)} -> ${path.join(filepath.dirname, filepath.basename+filepath.extname)}`);
}

// pugコンパイル
function pugTask(){
  return gulp
    .src(paths.src.pug)
    .pipe(
      plumber({ errorHandler: notify.onError("Error: <%= error.message %>") })
    )
    .pipe(rename(renamePath))

    .pipe(data(function(file){
      //file.path はフルパスなので、ルートディレクトリで区切って、拡張子も切る。
      const filepath = file.path.split('src/pug')[1].replace(/\.pug|\.html|index/g,'');
      console.log(`processing: ${filepath}`);

      //戻り値は、dataとしてpugに渡される。 pug的はそれをlocalsにセットする。
      return {
        //簡単に書くための関数たちを生成する。引数はその設定ファイル
        FILE_PATH:filepath, // /events/hoge
        IS_TEST,
        IS_PRODUCTION,
      };
    }))
    .pipe(pug({
      pretty: true,
      basedir: "src/pug"
    }))
    //change
    .pipe(gulp.dest(paths.dest.html));
}

function watch(done){
  gulp.watch(paths.src.pug[0], gulp.series(pugTask, bsReload) );
  gulp.watch(paths.src.sass,   gulp.series(sassTask, bsReload) );

  done();
}

//
//ブラウザリロード
//
function bsReload(done){
  browserSync.reload();
  done();
}

function browserSyncTask(done){
  browserSync({
    server: {
      baseDir: "./public/",       //対象ディレクトリ
      index  : "index.html"      //インデックスファイル
    }
  });
  done();
}

exports.build_test = 
  gulp.series(
    settingTaskFactory({"isTest": true}),
    gulp.parallel(sassTask, pugTask)
  );

exports.dev_test =  
  gulp.series(
    settingTaskFactory({"isTest": true}),
    gulp.parallel(sassTask, pugTask), 
    gulp.parallel(browserSyncTask, watch) 
  );


exports.build = exports.build_test;
exports.dev = exports.dev_test;

exports.build_production = 
  gulp.series(
    settingTaskFactory({"isTest": false}),
    gulp.parallel(sassTask, pugTask)
  );

exports.dev_production =  
  gulp.series(
    settingTaskFactory({"isTest": false}),
    gulp.parallel(sassTask, pugTask), 
    gulp.parallel(browserSyncTask, watch) 
  );

exports.default = exports.dev;