// 関数：スムーススクロール
// 指定したアンカー(#ID)へアニメーションでスクロール。url hashは変更しない
function scrollToAnker(id) {
  var headerHeight =  80; //スマホ版追従ヘッダーの高さ. PCでもそれぐらいの間が開くといいだろう
  var element = document.getElementById(id); 

  if(!element){
    return false;
  }

  var $element = $(element);
  
  var position = $element.offset().top - headerHeight;
  $('body,html').stop().animate( { scrollTop:position } , 500);

  return true;
}


//hashが(aリンク、手書きなどにより)変更され、#tab-から始まるタブだった場合にはタブを切り替える
window.addEventListener('hashchange', function(e) {
  var matchedHash = e.newURL.match(/#.*/)

  if(matchedHash.length <= 0 ){ // #がない場合。ありえないが。
    return; //なにもしない。
  }

  var hash = matchedHash[0];
  var matchedTab = hash.match(/^#tab-(.*)/);

  if(matchedTab){
    var tabId = matchedTab[1];
  
    //タブを切り替える

    console.log("opening");
    openTab(tabId)
  }
}, false);



//アンカーリンクを踏んだときにスムーズスクロールをする機能。
//同一ページならば、そのままスクロール。 他ページからの直接リンクでも、同様にスクロールする。
//ヘッダーがあるため、そのヘッダーの高さを考慮してスクロールしてくれる。
//全ページに読み込まれることを想定している。

//ページロード時に、URLの#がある場合にはスクロールする。
function checkAnchor(){
  var urlHash = location.hash.slice(1); //#が含まれてるので除く

  $("body,html").stop().scrollTop(0);
  //ハッシュ値があればページ内スクロール

  if(urlHash){
    setTimeout(function(){
      //ロード時の処理を待ち、時間差でスクロール実行
      scrollToAnker(urlHash);
    }, 1000);

    var matchedHash = urlHash.match(/^tab-(.*)/);

    if (matchedHash) {
      //タブを切り替える
      var contentId = matchedHash[1];

      openTab(contentId)

    }
  }
}

// a要素のクリック時にセット
function setAnchorClickEvent(){
  //通常のクリック時
  $('a[href*="#"]').click(function(e) {
    //ページ内リンク先を取得

    var href = $(this).attr("href");
    var hash = href.split("#")[1];
    
    //スクロール実行。成功時はリンク無効化
    
    scrollToAnker(hash);


    //タブ切り替え用のアンカーか？
    var matchedHash = hash.match(/^tab-(.*)/);

    if(matchedHash) { //もしそうなら
      //タブを切り替える
      var contentId = matchedHash[1];

      openTab(contentId)
    }

    return false;
  });
}


////////// タブ切り替え //////////

function openTab(contentId){
  var $tabs = $(".c-url-tab>.e-tabs>.e-tab");
  $tabs.removeClass("is-active");

  // #hash を変えて、scrollを選べるように でできるようにする
  http://localhost:3000/kouza/miso/#tab-douga

  //URLを変更
  window.history.pushState({contentId: contentId}, "", 
    location.href.split("#")[0] + "#tab-"+contentId);

  $(".c-url-tab>.e-tabs>.e-tab#tab-"+contentId).addClass("is-active");

  
  $(".c-url-tab>.e-panel").removeClass("is-shown");
  $(".c-url-tab>.e-panel#panel-"+contentId).addClass("is-shown");

}


//DOMが読み込まれてからでないといけない処理
$(function(){ 
  //グロナビ
  {
    var $globalNav = $(".c-global-nav>.e-menu-button");
    window.toggleMenu = function togggleMenu(isOpen){
      $globalNav.toggleClass("is-open", isOpen);
    }

    $(".js-open-menu").click(function(){
      toggleMenu();
  });
  }



  //URLのハッシュ値を取得
  checkAnchor();
  setAnchorClickEvent();

})